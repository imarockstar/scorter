<?php include 'inc/head.php'; 
	  include 'inc/header.php';
?>

  

    <!-- Header -->
    <header class="masthead bg-primary text-white text-center">
      <div class="container">
       
        <h1 class="text-uppercase mb-0">Sets</h1>
        <hr class="star-light">
        <h2 class="font-weight-light mb-0">Lift - Gain - Impress</h2>
      </div>
    </header>
    
    
    
    <!-- new set container start -->
    <div class="container">
	    
	<!-- row start -->
	<div class="row">
	
	<div class="col-lg-3">
		<img src="img/pp1.png" alt="profile picture place holder" class="img-fluid" />
	</div>
	
	<div class="col-lg-9">
		<div class="form-group">
    	<textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
  	</div>
	</div>
	
	</div>
	<!-- row end -->
	
    </div>
    <!-- new set container start -->
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 

    

    <!-- Contact Section -->
    <section id="contact">
      <div class="container">
        <h2 class="text-center text-uppercase text-secondary mb-0">Contact Me</h2>
        <hr class="star-dark mb-5">
        <div class="row">
          <div class="col-lg-8 mx-auto">
            <!-- To configure the contact form email address, go to mail/contact_me.php and update the email address in the PHP file on line 19. -->
            <!-- The form should work on most web servers, but if the form is not working you may need to configure your web server differently. -->
            <form name="sentMessage" id="contactForm" novalidate="novalidate">
              <div class="control-group">
                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                  <label>Name</label>
                  <input class="form-control" id="name" type="text" placeholder="Name" required="required" data-validation-required-message="Please enter your name.">
                  <p class="help-block text-danger"></p>
                </div>
              </div>
              <div class="control-group">
                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                  <label>Email Address</label>
                  <input class="form-control" id="email" type="email" placeholder="Email Address" required="required" data-validation-required-message="Please enter your email address.">
                  <p class="help-block text-danger"></p>
                </div>
              </div>
              <div class="control-group">
                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                  <label>Phone Number</label>
                  <input class="form-control" id="phone" type="tel" placeholder="Phone Number" required="required" data-validation-required-message="Please enter your phone number.">
                  <p class="help-block text-danger"></p>
                </div>
              </div>
              <div class="control-group">
                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                  <label>Message</label>
                  <textarea class="form-control" id="message" rows="5" placeholder="Message" required="required" data-validation-required-message="Please enter a message."></textarea>
                  <p class="help-block text-danger"></p>
                </div>
              </div>
              <br>
              <div id="success"></div>
              <div class="form-group">
                <button type="submit" class="btn btn-primary btn-xl" id="sendMessageButton">Send</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>

    <?php include 'inc/footer_content.php'; ?>

<?php include 'inc/footer.php'; ?>
