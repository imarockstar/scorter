<?php include 'inc/head.php'; 
	  include 'inc/header.php';
?>




<!-- Contact Section -->
    <section id="contact" class="push">
      <div class="container">
        <h2 class="text-center text-uppercase text-secondary mb-0">Search For A Band</h2>
        <hr class="star-dark mb-5">
        <div class="row">
          <div class="col-lg-8 mx-auto">
            <!-- To configure the contact form email address, go to mail/contact_me.php and update the email address in the PHP file on line 19. -->
            <!-- The form should work on most web servers, but if the form is not working you may need to configure your web server differently. -->
            <form action="lastfm_results.php" method="post">
              <div class="control-group">
                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                  <label>Artist</label>
                  <input class="form-control" id="nband" type="text" name="band" placeholder="Please enter a bands name" required="required" data-validation-required-message="Please enter a bands name">
                  <p class="help-block text-danger"></p>
                </div>
              </div>
              <br>
              <div id="success"></div>
              <div class="form-group">
	              <input type="submit" value="Go Fetch!" name="submit" class="btn btn-primary btn-xl">
                <!-- <button type="submit" class="btn btn-primary btn-xl" id="sendMessageButton">Go Fetch!</button> -->
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>

   

    

    
    

    
<?php 
	include 'inc/footer_content.php'; 
	include 'inc/footer.php';
?>
